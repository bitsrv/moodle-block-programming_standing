<?php
 
class block_programming_standing_edit_form extends block_edit_form {
 
    protected function specific_definition($mform) {
        // Fields for editing HTML block title and contents.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));

        $mform->addElement('text', 'config_listhowmany', get_string('showhowmanyonlist', 'block_programming_standing'));
        $mform->setDefault('config_listhowmany', 20);
        $mform->setType('config_listhowmany',PARAM_INT);
        $mform->addElement('text', 'config_perpageonfulllist', get_string('perpageonfulllist', 'block_programming_standing'));
        $mform->setDefault('config_perpageonfulllist', 50);
        $mform->setType('config_perpageonfulllist',PARAM_INT);
        $mform->addElement('text', 'config_shownames', get_string('shownames', 'block_programming_standing'));
        $mform->setDefault('config_shownames', 50);
        $mform->setType('config_shownames',PARAM_INT);
        $mform->addElement('text', 'config_wrongsubmitminutes', get_string('howmanyminuteswrongsubmit', 'block_programming_standing'));
        $mform->setDefault('config_wrongsubmitminutes', 120);
        $mform->setType('config_wrongsubmitminutes',PARAM_INT);
        $mform->addElement('text', 'config_showdetail', get_string('showdetail', 'block_programming_standing'));
        $mform->setDefault('config_showdetail', 1);
        $mform->setType('config_showdetail',PARAM_BOOL);

    }

    function set_data($defaults) {
        parent::set_data($defaults);

    }

}
